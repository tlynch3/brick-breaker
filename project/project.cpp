// Thomas Lynch, Jill Ybanez Final Project
// project.cpp

#include <iostream>
#include <unistd.h>
#include <stdlib.h>
#include <time.h>
#include <math.h>
#include "bricks.h"
#include "gfx3.h"
using namespace std;

// Function prototypes 
void welcomeScreen(int width, int height);
void instructScreen(int width, int height);
void printBricks(int width, int height, Bricks brickBoard);
void startGame(Bricks& brickBoard, int width, int height, double& xc, double& yc, double& dx, double& dy, int& oldBar);
void moveBar(int width, int barHeight, int& newBar, int& oldBar);
void breakBrick(double xc, double yc, double& dx, double& dy, int rad, Bricks& brickBoard);
void barBounce(double xc, double yc, double& dx, double& dy, int oldBar, int rad);
void deathScreen(int width, int height);
bool victory(Bricks brickBoard, int width, int height);


int main() {
  Bricks brickBoard;
  // Initializing the board x and y values and player number
  int width = gfx_screenwidth(), height = gfx_screenheight();
  gfx_open(width, height, "Brick Breaker");

  char c;
  double xc, yc; // x and y positions of the ball 
  int rad = 8;
  double dx = sqrt(32), dy = sqrt(32); // Speeds of ball
  int oldBar, newBar, change = 20; // Bar movement
  double barHeight = height - 130; // Set bar height
  int deathCount = 0; // counts how many times the user let the ball drop 
  bool check; 
  
  // Creating welcome screen
  welcomeScreen(width, height);

  // instructscreen(width, height);
  instructScreen(width, height);

  // Initializing the screen
  startGame(brickBoard, width, height, xc, yc, dx, dy, oldBar);

  // Will run till break occurs
  while (true) {
    // Filling in the previous circle
    gfx_color(0, 0, 0);
    gfx_fill_circle(xc, yc, rad);
    // Add next iteration
    xc = xc + dx;
    yc = yc + dy;
    // Print the next ball
    gfx_color(255, 255, 255);
	gfx_fill_circle(xc, yc, rad);
    usleep(500);
    // Waiting for user inputs
    if (gfx_event_waiting()) {
      c = gfx_wait();
      // Moving bar within arrow keys
      if (c == 'Q' || c == 'S') {
        (c == 'Q') ? (newBar = oldBar - change) : (newBar = oldBar + change);
        moveBar(width, barHeight, newBar, oldBar);
      }
      // Quitting the program
      if (c == 'q')
        break;
    }
    // Ball bouncing off of bar
    if ((yc + dy + rad >= barHeight))
      barBounce(xc, yc, dx, dy, oldBar, rad);
    // Breaking bricks
    breakBrick(xc, yc, dx, dy, rad, brickBoard);
    // Wall bouncing
    if (xc + dx >= width - rad)
      dx = -dx;
    if (xc + dx <= rad)
      dx = -dx;
    if (yc + dy <= rad)
      dy = -dy;
    // Restarting code when ball is missed and leading to end game once all three lives are used
    if (yc + dy >= height - 130 + BRICK_HEIGHT/3) {
      deathCount++;
      if (deathCount == 3) {
        deathScreen(width, height);
        break;
      }
      // Setting board with appropriate blocks missing
      startGame(brickBoard, width, height, xc, yc, dx, dy, oldBar);
    }

    // Checking for the completion of the game
    check = victory(brickBoard, width, height);
    if (check)
      break;
  }
  return 0;
}


// Welcome screen 
void welcomeScreen(int width, int height) {
  int altcount = 0;
  bool alt = true;
  char c;

  while (true) {
    gfx_changefont((char *) "r16");
    gfx_text(width/2 - gfx_textpixelwidth((char *) "Created by Thomas Lynch and Jill Ybanez", (char *) "r16")/2, height/4 + 300, "Created by Thomas Lynch and Jill Ybanez");

    gfx_changefont((char *) "r24");
    gfx_text(width/2 - gfx_textpixelwidth((char *) "Brick Breaker", (char *) "r24")/2, height/4, "Brick Breaker");

   // Blinking text and quit out
    while (true) {
      if (gfx_event_waiting()) {
        c = gfx_wait();
        //if (c >= 97 && c <= 122)
        if (altcount >= 6) // To speed up process, having the movement to the next screen automatic
          break;
      }
      // "Blinking" through the use of color changing
      (alt) ? gfx_color(255, 255, 255) : gfx_color(0, 0, 0);

      gfx_text(width/2 - gfx_textpixelwidth((char *) "Press Any Key", (char *) "r24")/2, height/4 + 150, "Press Any Key");
      // Alternating for color differences and wait time
      (alt) ? alt = false : alt = true;
      altcount++;
      usleep(750000);
    }
    gfx_clear();
    break;
  }
}


// Instruction screen quit out with the input of q
void instructScreen(int width, int height) {
  char c;
  while (true) {
    gfx_color(255, 255, 255);
    gfx_changefont((char *) "r24");
    gfx_text(width/2 - gfx_textpixelwidth((char *) "Instructions", (char *) "r24")/2, height/2 - 200, "Instructions");
    gfx_changefont((char *) "r16");
    gfx_text(width/2 - gfx_textpixelwidth((char *) "Move the bar with right and left arrow keys.", (char *) "r16")/2, height/2 - 100, "Move the bar with right and left arrow keys.");
    gfx_text(width/2 - gfx_textpixelwidth((char *) "You win after all bricks are broken.", (char *) "r16")/2, height/2 - 50, "You win after all bricks are broken.");
    gfx_text(width/2 - gfx_textpixelwidth((char *) "You will have three lives. Good Luck.", (char *) "r16")/2, height/2, "You will have three lives. Good Luck.");
    gfx_text(width/2 - gfx_textpixelwidth((char *) "The ball gets slightly faster with every broken brick.", (char *) "r16")/2, height/2 + 50, "The ball gets slightly faster with every broken brick.");
    gfx_text(width/2 - gfx_textpixelwidth((char *) "Press c to Contnue.", (char *) "r16")/2, height/2 + 100, "Press c to Continue.");

    c = gfx_wait();
    if (c == 'c'){
      gfx_clear();
      break;
    }
  }
}


// Printing out the bricks that hold a true value, as well as the initial position for the bar and ball
void printBricks(int width, int height, Bricks brickBoard) {
  // Initializing the colored bricks once, becuase they will be turned off when hit
  for (int i = 0; i < BRICK_ROW; i++) {
    gfx_color(255 - (i+1)*(255/5), 0, 0 + (i+1)*(255/5));
    for (int j = 0; j < BRICK_NUM; j++)
       if (!(brickBoard.empty(i, j)))
        gfx_fill_rectangle(MRGN + j*(BRICK_LENGTH + BRICK_SPACE), 2*MRGN + i*(BRICK_HEIGHT + 2*BRICK_SPACE), BRICK_LENGTH, BRICK_HEIGHT);
  }
  // Printing out initial position of bar
  gfx_color(216, 70, 252);
  gfx_fill_rectangle(width/2 - BRICK_LENGTH * .75, height - 130, BRICK_LENGTH * 1.5, BRICK_HEIGHT/3);
  // Printing out initial position of ball
  gfx_color(255, 255, 255);
  gfx_fill_circle(width/2, 600, 8);
}


// Function to start of the program, as well as reinitialize after a death
void startGame(Bricks& brickBoard, int width, int height, double& xc, double& yc, double& dx, double& dy, int& oldBar) {
  double delta;
  char c;
  bool alt = true;
  gfx_clear();
  printBricks(width, height, brickBoard);
  // Initializing bar and ball starting values
  xc = width/2;
  yc = 600;
  oldBar = width/2;
  // Initializing a random value for x and y velocity
  srand(time(NULL));
  delta = sqrt(dx*dx + dy*dy);
  while (dx == 0)
    dx = rand() % 15 - 7;
  dy = -1*sqrt(delta*delta - dx*dx);
  // Holding till user is ready
  while (true) {
    if (gfx_event_waiting()) {
      c = gfx_wait();
      if (c == 'Q' || c == 'S')
        break;
    }
    gfx_color(255, 255, 255);
    gfx_changefont((char *) "r24");
    gfx_text(width/2 - gfx_textpixelwidth((char *) "Press <- or ->", (char *) "r24")/2, 550, "Press <- or ->");
  }
  // Ensure the text disappears
  gfx_color(0, 0, 0);
  gfx_changefont((char *) "r24");
  gfx_text(width/2 - gfx_textpixelwidth((char *) "Press <- or ->", (char *) "r24")/2, 550, "Press <- or ->");
}


// Moving the bar through the use of the left and right arrow key
void moveBar(int width, int barHeight, int& newBar, int& oldBar) {
  if ((newBar + BRICK_LENGTH * .75 < width) && (newBar - BRICK_LENGTH * .75 > 0)) {
    // "Erasing" previous bar
    gfx_color(0, 0, 0);
    gfx_fill_rectangle(oldBar - BRICK_LENGTH * .75, barHeight, BRICK_LENGTH * 1.5, BRICK_HEIGHT/3);
    // Drawing new bar position
    gfx_color(216, 70, 252);
    gfx_fill_rectangle(newBar - BRICK_LENGTH * .75, barHeight, BRICK_LENGTH * 1.5, BRICK_HEIGHT/3);
    // Setting new position
    oldBar = newBar;
  }
  else
    (newBar + BRICK_LENGTH * .75 > width) ? (newBar = width - BRICK_LENGTH * .75) : (newBar = BRICK_LENGTH * .75);
}


// Checking for the collision with a brick, changing velocity, and placing a black brick over top
void breakBrick(double xc, double yc, double& dx, double& dy, int rad, Bricks& brickBoard) {
  double speedUp = 51./50;
  
  for (int i = 0; i < BRICK_ROW; i++) {
    // Bottom brick check
    if ((yc + dy - rad <= 2*MRGN + (i+1)*BRICK_HEIGHT + 2*i*BRICK_SPACE) && (yc + dy - rad >= 2*MRGN + (i+1)*BRICK_HEIGHT + 2*i*BRICK_SPACE - 2*rad))
      for (int j = 0; j < BRICK_NUM; j++)
        if ((xc + dx + rad >= MRGN + j*(BRICK_LENGTH + BRICK_SPACE)) && (xc + dx - rad <= MRGN + (j+1)*BRICK_LENGTH + j*BRICK_SPACE))
          if (!brickBoard.empty(i, j)) {
            dy = -dy * speedUp;
            dx = dx * speedUp;
            brickBoard.change(i, j);
            gfx_color (0, 0, 0);
            gfx_fill_rectangle(MRGN + j*(BRICK_LENGTH + BRICK_SPACE), 2*MRGN + i*(BRICK_HEIGHT + 2*BRICK_SPACE), BRICK_LENGTH, BRICK_HEIGHT);
          }
    // Top brick check
    if ((yc + dy + rad >= 2*MRGN + i*BRICK_HEIGHT + 2*i*BRICK_SPACE) && (yc + dy + rad <= 2*MRGN + i*BRICK_HEIGHT + 2*i*BRICK_SPACE + 2*rad))
      for (int j = 0; j < BRICK_NUM; j++)
        if ((xc + dx + rad >= MRGN + j*(BRICK_LENGTH + BRICK_SPACE)) && (xc + dx - rad <= MRGN + (j+1)*BRICK_LENGTH + j*BRICK_SPACE))
          if (!brickBoard.empty(i, j)) {
            dy = -dy * speedUp;
            dx = dx * speedUp;
            brickBoard.change(i, j);
            gfx_color (0, 0, 0);
            gfx_fill_rectangle(MRGN + j*(BRICK_LENGTH + BRICK_SPACE), 2*MRGN + i*(BRICK_HEIGHT + 2*BRICK_SPACE), BRICK_LENGTH, BRICK_HEIGHT);
          }
  }

  for (int i = 0; i < BRICK_NUM; i++) {
    // Left side brick check
    if ((xc + dx + rad >= MRGN + i*(BRICK_LENGTH + BRICK_SPACE)) && (xc + dx + rad <= 2*MRGN + i*(BRICK_LENGTH + BRICK_SPACE) + 2*rad))
      for (int j = 0; j < BRICK_ROW; j++)
        if ((yc + dy + rad >= 2*MRGN + j*(BRICK_HEIGHT + 2*BRICK_SPACE)) && (yc + dy - rad <= 2*MRGN + (j+1)*BRICK_HEIGHT + 2*j*BRICK_SPACE))
          if (!brickBoard.empty(j, i)) {
            dx = -dx * speedUp;
            dy = dy * speedUp;
            brickBoard.change(j, i);
            gfx_color (0, 0, 0);
            gfx_fill_rectangle(MRGN + i*(BRICK_LENGTH + BRICK_SPACE), 2*MRGN + j*(BRICK_HEIGHT + 2*BRICK_SPACE), BRICK_LENGTH, BRICK_HEIGHT);
          }
    // Right side brick check
    if ((xc + dx - rad <= MRGN + (i+1)*BRICK_LENGTH + i*BRICK_SPACE) && (xc + dx - rad >= MRGN + (i+1)*BRICK_LENGTH + i*BRICK_SPACE - 2*rad))
      for (int j = 0; j < BRICK_ROW; j++)
        if ((yc + dy + rad >= 2*MRGN + j*(BRICK_HEIGHT + 2*BRICK_SPACE)) && (yc + dy - rad <= 2*MRGN + (j+1)*BRICK_HEIGHT + 2*j*BRICK_SPACE))
          if (!brickBoard.empty(j, i)) {
            dx = -dx * speedUp;
            dy = dy * speedUp;
            brickBoard.change(j, i);
            gfx_color (0, 0, 0);
            gfx_fill_rectangle(MRGN + i*(BRICK_LENGTH + BRICK_SPACE), 2*MRGN + j*(BRICK_HEIGHT + 2*BRICK_SPACE), BRICK_LENGTH, BRICK_HEIGHT);
          }
  }
}


// Ball bouncing off of the the bar
void barBounce(double xc, double yc, double& dx, double& dy, int oldBar, int rad) {
  double delta;
  if ((xc + dx - rad <= oldBar + BRICK_LENGTH * .75) && (xc + dx + rad >= oldBar - BRICK_LENGTH * .75)) {
    delta = sqrt(dy*dy + dx*dx);
    dx = ((xc + dx) - oldBar + 5)/(BRICK_LENGTH * 1.5) * 15;
    if (dx*dx >= delta*delta)
      dx = delta - .5;
    dy = -1*sqrt(delta*delta - dx*dx);
    if (dx == 0 || dy == 0)
      (dx == 0) ? dx = 1 : dy = 1;
  }
}


// Death screen after 3 deaths occur
void deathScreen(int width, int height) {
  char c;
  gfx_clear();
  while (true) {
    gfx_color(190, 35, 20);
    gfx_changefont((char *) "r24");
    gfx_text(width/2 - gfx_textpixelwidth((char *) "You Lose", (char *) "r24")/2, height/2, "You Lose");
    gfx_text(width/2 - gfx_textpixelwidth((char *) "Press q to exit", (char *) "r24")/2, height/2 + 100, "Press q to exit");
    c = gfx_wait();
    if (c == 'q')
      break;
  }
}


// Once all bricks disappear from the screen display victory
bool victory(Bricks brickBoard, int width, int height) {
  char c;
  if (brickBoard.complete()) {
    gfx_clear();
    while (true) {
      gfx_color(20, 180, 60);
      gfx_changefont((char *) "r24");
      gfx_text(width/2 - gfx_textpixelwidth((char *) "You Win", (char *) "r24")/2, height/2, "You Win");
      gfx_text(width/2 - gfx_textpixelwidth((char *) "Press q to exit", (char *) "r24")/2, height/2 + 100, "Press q to exit");
      c = gfx_wait();
      if (c == 'q')
        break;
    }
    return true;
  }
  else
    return false;
}
