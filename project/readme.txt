Lab 12 - Final Project - Brick Breaker
Thoams Lynch & Jill Ybanez
readme.txt

P1: Thomas Lynch, tlych3
P2: Jill Ybanez, jybanez1

Notes:
We ran into some difficulty within our gfx_wait commands. Due to the slowness of the ssh client, waiting for an inputed range for the c character, to be able to take all keys into account, cause a large delay within the program, leading to an overall slowing of the code. To counter this, we simply took out the limited range of values to allow for any interaction to progress the program.
To solve for the slowness that would occur if the screen was clear after every iteration, we decided to forgo the gfx_clear() and instead change the previous iterations to match the color of the background.
If there is a lot of difficulty moving on to the next screen, one solution that we found to work, was to run the executable multiple times, oddly enough we found that on average, the move times we ran the executable since the previous compilation the less we had to wait for progression. 
