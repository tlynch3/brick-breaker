// Thomas Lynch, Jill Ybanez Final Project
// bricks.cpp
// implementation for the Bricks class

#include <iostream>
#include <string>
#include <cstdlib>
using namespace std;

#include "bricks.h"

// Constructor
Bricks::Bricks() {
  for (int i = 0; i < BRICK_ROW; i++)
    for (int j = 0; j < BRICK_NUM; j++)
      board[i][j] = true;
}

// Changes a brick from empty to full or vice versa 
void Bricks::change(int x, int y) {
  (board[x][y]) ? (board[x][y] = false) : (board[x][y] = true);
}

// Boolean method to check if added point is empty
bool Bricks::empty(int x, int y) {
  return (board[x][y] == false);
}

// Boolean method to check for the fullness of the board, use for the end game to see if user broke all bricks 
bool Bricks::complete() { 
  for (int i = 0; i < BRICK_ROW; i++)
    for (int j = 0; j < BRICK_NUM; j++)
      if (!empty(i, j)) return false; // returns false if board is not empty 
  return true; // returns true if board is completely empty (meaning the user won the game) 
}

