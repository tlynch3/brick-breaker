// Thomas Lynch, Jill Ybanez Final Project
// bricks.h

// Setting up constants to make comparisons and board size easier
const int BRICK_ROW = 5; // number of rows of bricks 
const int BRICK_NUM = 9; // number of bricks per row 
const double BRICK_LENGTH = 118.7; 
const double BRICK_HEIGHT = 45;
const int BRICK_SPACE = 15; // space between bricks 
const int MRGN = 40;

class Bricks {
  public:
   // Constructor
   Bricks(); // initializes the brick board 

   // Utilities
   void change(int, int); // changes brick from empty to full or vice versa 
   bool empty(int, int); // checks if a brick is empty or not 
   bool complete(); // checks if user won the game (if the board has been completely cleared of bricks) 

  private:
   // Raw data
   bool board[BRICK_ROW][BRICK_NUM];
};
